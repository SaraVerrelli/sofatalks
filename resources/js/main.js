$(function () {
    //Home page

    // Admin page

    $(".approve-btn").on("click", function () {
        let id = $(this).attr("data-topic-id");
        $.ajax({
            method: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "admin/accept-topic",
            data: { id: id },
            success: function () {
                $("td[data-topic-id=" + id + "]")
                    .prev()
                    .children()
                    .replaceWith(
                        '<i class="text-success fa fa-solid fa-thumbs-up"></i>'
                    );
                $("td[data-topic-id=" + id + "]").replaceWith("<td></td>");
            },
            error: function () {},
        });
    });

    $("body").on("click", ".change_topic_btn", function () {
        let categories = [];
        let id = $(this).attr("data-topic-id");
        $(".topic_to_approve_card").each(function () {
            let catId = $(this).attr("data-category-id");
            categories.push(catId);
        });

        $.ajax({
            method: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "admin/change-topic",
            data: { id: id, categories: categories },
            success: function (data) {
                let topic = data.topic;
                let category = data.category;
                $("#" + data.id + ".topic_to_approve_card")
                    .html(` <h4 class="my-2 p-3">${topic["title"]}</h4>
              <h5 class="ms-2">
                  <span class="badge"
                      style="background-color:${category["colour"]};">${category["name"]}</span>
                  <span class="float-end change_topic_btn" data-topic-id="${topic["id"]}"><i
                          class="fas fa-redo"></i></span>
              </h5>`);

                $("#" + data.id + ".topic_to_approve_card").attr(
                    "data-category-id",
                    category["id"]
                );
                $("#" + data.id + ".topic_to_approve_card").attr(
                    "id",
                    topic["id"]
                );
            },

            error: function () {
                console.log("ma che ne so");
            },
        });
    });

    $(".pick-topic-card").on("click", function () {
        $(".pick-topic-card").removeClass("selected-card");
        $(this).addClass("selected-card");
        let id = $(this).attr("id");
        $("#selected_topic").val(id);
        $("#vote-btn").removeClass("disabled");
    });

    $("#approve_topics_btn").on("click", function () {
        let topics_id = [];
        $(".topic_to_approve_card").each(function () {
            let id = $(this).attr("id");
            topics_id.push(id);
        });

        $.ajax({
            method: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            url: "admin/approve-topics",
            data: { topics_id: topics_id },
            success: function (data) {
                location.reload();
            },
            error: function () {
                console.log("error");
            },
        });
    });
});
