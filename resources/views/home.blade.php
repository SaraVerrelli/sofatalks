<x-layout>
    <header class="container-fluid mt-5">
        <div class="row mx-auto">
            <div class="col-6 pb-2">
                <h1 class="pt-5 fw-bolder fs-title mb-3">SofaTalks club.</h1>
                <p class="border-4 border-start border-warning ps-3 text-muted fs-4 py-1 my-5">SofaTalks is an English
                    zoom
                    chat
                    room where we discuss different topics of today's
                    society. We want to hear your opinion!</p>
                @guest
                    <a href="{{ route('login') }}" class="fs-5 mx-3 mt-5 btn white bg-acc">Log in</a>
                    <a href="{{ route('register') }}" class="fs-5 mx-3 mt-5 btn white bg-acc">Sign up</a>

                @endguest
            </div>
            <div class="col-6 text-center">
                <video width="70%" autoplay muted type="video/mp4" src="/media/welcome.MP4"></video>
            </div>
        </div>




    </header>
    <div class="container-fluid bg-secondary bg-opacity-10">
        <div class="row mx-auto py-5">
            <div class="col-12 col-md-5 mt-5">
                @if (session('percentage'))
                    <div class="alert alert-success">
                        {{ session('percentage') }}
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (empty($topics_to_vote))
                    <h5>The topics for our next discussion will be published on Monday</h5>
                @else
                    <h4 class="text-center">Pick your favourite topic for our next discussion</h4>
                    @foreach ($topics_to_vote as $topic)
                        <div id="{{ $topic->id }}" class="card my-3 shadow pick-topic-card">
                            <div class="card-body">
                                <h5 class="card-title text-center">{{ $topic->title }}</h5>
                                <p class="card-text text-center">{{ $topic->description }}</p>
                                <h5 class="text-center">
                                    <span class="badge"
                                        style="background-color:{{ $topic->category->colour }};">{{ $topic->category->name }}</span>
                                </h5>
                            </div>
                        </div>
                    @endforeach
                    <div class="row justify-content-center">
                        <form method="POST" class="text-center" action="{{ route('store-vote') }}">
                            @csrf
                            <input id="selected_topic" type="hidden" name="voted_topic">
                            <button type="submit" id="vote-btn"
                                class="btn btn-primary scale1h w-auto btn-lg disabled">Vote</button>

                        </form>
                    </div>
                @endif
            </div>



            <div class="col-12 col-md-6 my-auto offset-1 p-3">
                <h3>Lorem ipsum dolor sit amet consectetur</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. In obcaecati sequi quidem laboriosam ex odio
                    officia, ea fuga perferendis impedit voluptates nostrum ad! Iste exercitationem eos repudiandae modi
                    a doloribus?</p>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptates beatae nemo libero nam saepe
                    dicta eos soluta aliquam, labore necessitatibus minus harum quia deleniti quasi iure amet a
                    recusandae. Nemo.</p>
                <div class="row justify-content-center">
                    <a href="https://discord.com/channels/809513778563514369/822090981398413352" target="_blank"
                        class="btn btn-success w-auto btn-lg {{ date('l') != 'Thursday' ? 'disabled' : 'scale1h' }} ">Join
                        us!</a>
                    @if (date('l') != 'Thursday')
                        <span class="text-center mt-3 text-muted">Join us every Thursday evening on Discord!</span>
                    @endif

                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid">


        <div class="row mx-auto py-3 mb-5">
            <h2 class="text-center my-3">Lorem ipsum</h2>
            <div class="col-12 col-md-4 p-5">
                <div class="card shadow scale1h">
                    <div class="card-body">
                        <h4 class="card-title text-center">Special title treatment</h5>
                            <span class="justify-content-center d-flex my-5">
                                <i class="fa fa-3x fa-brands fa-wpexplorer"></i>
                            </span>
                            <h5 class="text-center fw-bold text-info">bla bla bla</h5>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 p-5">
                <div class="card shadow scale1h">
                    <div class="card-body">
                        <h4 class="card-title text-center">Special title treatment</h5>
                            <span class="justify-content-center d-flex my-5">
                                <i class="fa fa-3x fa-brands fa-wpexplorer"></i>
                            </span>
                            <h5 class="text-center fw-bold text-danger">bla bla bla</h5>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 p-5">
                <div class="card shadow scale1h">
                    <div class="card-body">
                        <h4 class="card-title text-center">Special title treatment</h5>
                            <span class="justify-content-center d-flex my-5">
                                <i class="fa fa-3x fa-brands fa-wpexplorer"></i>
                            </span>
                            <h5 class="text-center fw-bold text-warning">bla bla bla</h5>
                    </div>
                </div>
            </div>
        </div>



    </div>


</x-layout>
