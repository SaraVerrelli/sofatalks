<x-layout>
    <header class="container-fluid py-4">
        <div class="row mx-auto">
            <h1 class="text-center">With great power comes great responsibility</h1>
        </div>

    </header>

    <main class="container-fluid">
        <div class="row mx-auto">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="d-flex justify-content-center">
                    <h2>The topics for next week:</h2>
                </div>
                <div class="row mx-auto" id="topics_div">
                    @if ($to_vote == 2)
                        <h5>Great, we have the topics for the next discussion!</h5>
                    @endif
                    @foreach ($picked_topics as $topic)
                        <div id="{{ $topic->id }}" class="card shadow my-2 h-100 p-3 pt-0 topic_to_approve_card"
                            data-category-id="{{ $topic->category_id }}">
                            <h4 class="my-2 p-3 {{ $to_vote == 2 ? 'text-center' : '' }}">{{ $topic->title }}</h4>
                            <h5 class="ms-2 {{ $to_vote == 2 ? 'text-center' : '' }}">
                                <span class="badge"
                                    style="background-color:{{ $topic->category->colour }};">{{ $topic->category->name }}</span>
                                @if ($to_vote == 1)
                                    <span class="float-end change_topic_btn" data-topic-id="{{ $topic->id }}"><i
                                            class="fas fa-redo"></i></span>
                                @endif
                            </h5>
                        </div>
                    @endforeach
                    @if ($to_vote == 1)
                        <a id="approve_topics_btn"
                            class="btn btn-lg btn-outline-warning w-auto mx-auto my-3">Approve</a>
                    @endif
                </div>

            </div>

        </div>
        <div class="row mx-auto">
            <div class="col-10 offset-1">
                <h3>Users</h3>
                <div class="container_overflow">
                    <table class="table table-hover table-success table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Admin?</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    @if ($user->is_admin)
                                        <td class="fw-bold text-success">YES</td>
                                    @else
                                        <td class="fw-bold text-danger">NO</td>
                                    @endif

                                </tr>
                            @endforeach
                            </tr>
                        </tbody>

                    </table>
                </div>

            </div>
        </div>
        <div class="row mx-auto my-5">
            <div class="col-10 offset-1">
                <h3>Topics</h3>
                <div id="container_overflow">
                    <table class="table table-hover table-info table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>User</th>
                                <th>Created at</th>
                                <th>Category</th>
                                <th class="text-center">Accepted</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($topics as $topic)
                                <tr>
                                    <td>{{ $topic->id }}</td>
                                    <td>{{ $topic->title }}</td>
                                    <td>{{ $topic->description }}</td>
                                    <td>{{ $topic->user->email }}</td>
                                    <td>{{ $topic->created_at }}</td>
                                    <td>{{ $topic->category->name }}</td>
                                    @if ($topic->is_accepted)
                                        <td class="text-center"><i class="text-success fa fa-solid fa-thumbs-up"></i>
                                        </td>
                                        <td></td>
                                    @else
                                        <td class="text-center"><i
                                                class="text-danger fa fa-solid fa-thumbs-down"></i>
                                        </td>
                                        <td data-topic-id="{{ $topic->id }}" class="approve-btn">
                                            <span class="btn btn-outline-primary">
                                                <i class="fa fa-solid fa-check"></i>
                                            </span>
                                        </td>
                                    @endif

                                </tr>
                            @endforeach
                            </tr>
                        </tbody>

                    </table>
                </div>

            </div>
        </div>

    </main>





</x-layout>
