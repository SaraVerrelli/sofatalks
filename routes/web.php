<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\VoteController;
use App\Http\Middleware\Admin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/about', [PublicController::class, 'about'])->name('about');
Route::get('/topic/create', [TopicController::class, 'create'])->name('newTopic');
Route::post('/topic/store', [TopicController::class, 'store'])->name('storeTopic');
Route::post('/vote-for-topic', [VoteController::class, 'store'])->name('store-vote');





// Admin 

Route::middleware(['auth.admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/', [AdminController::class, 'dashboard'])->name('admin.dashboard');
        Route::post('/accept-topic', [AdminController::class, 'acceptTopic'])->name('accept-topic');
        Route::any('/change-topic', [AdminController::class, 'changeTopic'])->name('change-topic');
        Route::post('/approve-topics', [AdminController::class, 'approveTopics'])->name('approve-topics');
    });
});
