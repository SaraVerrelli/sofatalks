<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PublicController extends Controller
{
    public function home()
    {

        $topics_to_vote = Topic::where('to_vote', 2)->first();
        if ($topics_to_vote) {
            $topics_to_vote = Topic::where('to_vote', 2)->get();
        } else {
            $topics_to_vote = null;
        }

        return view('home', compact('topics_to_vote'));
    }

    public function about()
    {
        return view('about');
    }
}
