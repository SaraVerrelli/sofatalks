<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    public function dashboard()
    {
        $users = User::all();
        $topics = Topic::orderBy('created_at', 'DESC')->get();
        $picked_topics = $this->getTopicsToPick();
        $to_vote = $picked_topics['to_vote'];

        if ($to_vote == 1) {
            $picked_topics = $picked_topics['picked_topics'];
        } else {
            $picked_topics = $picked_topics['alreadyApproved'];
        }


        return view('admin.index', compact('users', 'topics', 'picked_topics', 'to_vote'));
    }

    public function acceptTopic(Request $request)
    {

        $topic_id = $request->post()['id'];
        $topic = Topic::find($topic_id);
        $topic->is_accepted = true;
        return $topic->save();
    }

    public function getTopicsToPick()
    {

        $alreadyPicked = Topic::where('to_vote', 1)->first();
        $alreadyApproved = Topic::where('to_vote', 2)->first();

        if (empty($alreadyPicked) && empty($alreadyApproved)) {
            $categories = [];
            $suitableTopics = [];
            $topics_to_pick = Topic::where([['is_accepted', true], ['discussed', false]])->get();

            foreach ($topics_to_pick as $topic) {

                if (!in_array($topic->category_id, $categories)) {
                    array_push($categories, $topic->category_id);
                    array_push($suitableTopics, $topic->id);
                }
            }
            $picked_topics = Topic::whereIn('id', $suitableTopics)
                ->inRandomOrder()
                ->take(3)
                ->get();

            foreach ($picked_topics as $topic) {
                $topic->to_vote = 1;
                $topic->save();
            }
            $to_vote = 1;
            return compact('picked_topics', 'to_vote');
        } elseif ($alreadyPicked) {
            $picked_topics = Topic::where('to_vote', 1)->get();
            $to_vote = 1;
            return compact('picked_topics', 'to_vote');
        } else {
            $alreadyApproved = Topic::where('to_vote', 2)->get();
            $to_vote = 2;
            return compact('alreadyApproved', 'to_vote');
        }
    }

    public function changeTopic(Request $request)
    {
        $params = $request->post();
        $id = $params['id'];
        $oldTopic = Topic::find($id);
        $oldTopic->to_vote = 0;
        $oldTopic->save();
        $cat = Topic::find($id)->category_id;
        $categories = $params['categories'];
        $toDel = array_search($cat, $categories);
        unset($categories[$toDel]);
        $topic = Topic::where([['id', '!=', $id], ['is_accepted', 1]])->whereNotIn('category_id', $categories)->inRandomOrder()->take(1)->first();
        $topic->to_vote = 1;
        $topic->save();
        $categoryId = $topic->category_id;
        $category = Category::find($categoryId);
        return ['topic' => $topic, 'id' => $id, 'category' => $category];
    }



    public function approveTopics(Request $request)
    {
        $topics_id = $request->post()['topics_id'];
        $topics = Topic::whereIn('id', $topics_id)->get();
        foreach ($topics as $topic) {
            $topic->to_vote = 2;
            $topic->save();
        }
        return json_encode($topics);
    }
}
