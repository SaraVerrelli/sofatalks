<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Culture and arts',
                'colour' => '#FF8333'
            ],
            [
                'name' => 'Human rights',
                'colour' => '#FFDA33'
            ],
            [
                'name' => 'Relationships',
                'colour' => '#ED142C'
            ],
            [
                'name' => 'Science and technology',
                'colour' => '#ED14B1'
            ],
            [
                'name' => 'Politics',
                'colour' => '#907C7E'
            ],
            [
                'name' => 'Environment',
                'colour' => '#99FF33'
            ],
            [
                'name' => 'Religion',
                'colour' => '#101893'
            ],
            [
                'name' => 'Health',
                'colour' => '#33FFC1'
            ],
            [
                'name' => 'Psichology',
                'colour' => '#3383FF'
            ],
            [
                'name' => 'Society',
                'colour' => '#671093'
            ],

        ];



        foreach ($categories as $category) {
            DB::table('categories')->insert([
                'name' => $category['name'],
                'colour' => $category['colour'],
            ]);
        }
    }
}
